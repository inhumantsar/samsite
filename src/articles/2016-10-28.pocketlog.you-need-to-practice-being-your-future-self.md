Title: You Need to Practice Being Your Future Self
Date: 2016-10-27 20:05
Tags: pocketlog, links, 
Slug: you-need-to-practice-being-your-future-self
Authors: hbr.org
Etag: 

<div id="you-need-to-practice-being-your-future-self" class="pocketlog">
  <div><div class="excerpt-image">
      <img src="https://hbr.org/resources/images/article_assets/2016/03/mar16-28-158709140.jpg" alt="You Need to Practice Being Your Future Self" />
    </div>
  <p>
      I was coaching Sanjay,* a leader in a technology firm who felt stuck and frustrated. He wasn’t where he wanted to be at this point in his career.  He had come to our coaching session, as usual, prepared to discuss the challenges he was currently facing.
    </p>
    <p class="post-info">
      via <a href="https://hbr.org/2016/03/you-need-to-practice-being-your-future-self">hbr.org</a>
    </p>
  </div>
</div>