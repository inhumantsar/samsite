Title: Teslas will now be sold with enhanced hardware suite for full autonomy
Date: 2016-10-20 11:38
Tags: pocketlog, links, autonomous vehicles, tesla
Slug: teslas-will-now-be-sold-with-enhanced-hardware-suite-for-full-autonomy
Authors: arstechnica.com
Etag: 

<div id="teslas-will-now-be-sold-with-enhanced-hardware-suite-for-full-autonomy" class="pocketlog">
  <div><div class="excerpt-image">
      <img src="https://cdn.arstechnica.net/wp-content/uploads/2016/10/AutopilotNew-800x361.jpg" alt="Teslas will now be sold with enhanced hardware suite for full autonomy" />
    </div>
  <p>
      Late Wednesday, Tesla’s CEO Elon Musk announced that the company would be adding its own hardware to new all new Tesla cars to allow up to Level 5 autonomy. In the automotive industry, Level 5 denotes a fully self-driving vehicle.
    </p>
    <p class="post-info">
      via <a href="http://arstechnica.com/cars/2016/10/tesla-says-all-its-cars-will-ship-with-hardware-for-level-5-autonomy/">arstechnica.com</a>
    </p>
  </div>
</div>