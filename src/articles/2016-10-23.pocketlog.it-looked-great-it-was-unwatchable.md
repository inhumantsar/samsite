Title: It Looked Great. It Was Unwatchable.
Date: 2016-10-23 13:14
Tags: pocketlog, links, 
Slug: it-looked-great-it-was-unwatchable
Authors: www.slate.com
Etag: 

<div id="it-looked-great-it-was-unwatchable" class="pocketlog">
  <div><div class="excerpt-image">
      <img src="http://www.slate.com/content/dam/slate/articles/arts/movies/2016/10/161018_MOV_Billy-Lynn-Halftime-Walk.jpg.CROP.promo-xlarge2.jpg" alt="It Looked Great. It Was Unwatchable." />
    </div>
  <p>
      Ang Lee, the three-time Oscar-winning film director, did his best to lower expectations. “It’s kind of an experimental movie,” he said at the Friday night premiere of Billy Lynn’s Long Halftime Walk at the New York Film Festival.
    </p>
    <p class="post-info">
      via <a href="http://www.slate.com/articles/arts/movies/2016/10/billy_lynn_s_long_halftime_walk_looks_fantastic_it_s_also_unwatchable.html">www.slate.com</a>
    </p>
  </div>
</div>