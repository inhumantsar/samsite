Title: The Border : 92.05
Date: 2016-10-15 11:27
Tags: pocketlog, links, 
Slug: the-border-92-05
Authors: www.theatlantic.com
Etag: 

<div id="the-border-92-05" class="pocketlog">
  <div>
  <p>
      At age sixty-one, Gunaji is a trim, white-haired man with the accent of his native India and a severe disposition. He is an American success story -- an immigrant who rose to prominence. He has a Ph.D. and his résumé is ten pages long.
    </p>
    <p class="post-info">
      via <a href="http://www.theatlantic.com/past/docs/issues/92may/border.htm">www.theatlantic.com</a>
    </p>
  </div>
</div>