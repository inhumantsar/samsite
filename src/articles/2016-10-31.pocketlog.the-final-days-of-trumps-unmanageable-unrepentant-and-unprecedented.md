Title: The Final Days of Trump’s Unmanageable, Unrepentant, and Unprecedented Campaign
Date: 2016-10-31 12:26
Tags: pocketlog, links, 
Slug: the-final-days-of-trumps-unmanageable-unrepentant-and-unprecedented
Authors: nymag.com
Etag: 

<div id="the-final-days-of-trumps-unmanageable-unrepentant-and-unprecedented" class="pocketlog">
  <div><div class="excerpt-image">
      <img src="http://pixel.nymag.com/imgs/daily/intelligencer/2016/10/27/magazine/28-sherman-2.w710.h473.jpg" alt="The Final Days of Trump’s Unmanageable, Unrepentant, and Unprecedented Campaign" />
    </div>
  <p>
      “I’m on the battlefield right now, which is amazing,” Donald Trump said as he surveyed the Gettysburg National Military Park. “When you talk about historic, this is the whole ballgame.
    </p>
    <p class="post-info">
      via <a href="http://nymag.com/daily/intelligencer/2016/10/trump-campaign-final-days.html">nymag.com</a>
    </p>
  </div>
</div>