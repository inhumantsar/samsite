Title: Elon Musk provides more details about how first Mars colonists will live
Date: 2016-10-24 11:48
Tags: pocketlog, links, 
Slug: elon-musk-provides-more-details-about-how-first-mars-colonists-will-live
Authors: arstechnica.com
Etag: 

<div id="elon-musk-provides-more-details-about-how-first-mars-colonists-will-live" class="pocketlog">
  <div><div class="excerpt-image">
      <img src="https://cdn.arstechnica.net/wp-content/uploads/2016/10/GettyImages-610721250-800x534.jpg" alt="Elon Musk provides more details about how first Mars colonists will live" />
    </div>
  <p>
      When he delivered his Mars colonization presentation at the International Astronautical Conference in September, SpaceX founder Elon Musk spent a lot of time discussing the Interplanetary Transport System rocket and spacecraft, But he offered precious little information about what the firsts
    </p>
    <p class="post-info">
      via <a href="http://arstechnica.com/science/2016/10/geodesic-domes-and-tunneling-droids-musk-opens-up-about-mars-life/">arstechnica.com</a>
    </p>
  </div>
</div>