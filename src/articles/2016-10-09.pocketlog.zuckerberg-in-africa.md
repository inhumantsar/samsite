Title: Zuckerberg in Africa
Date: 2016-10-09 11:07
Tags: pocketlog, links, facebook, africa, tech
Slug: zuckerberg-in-africa
Authors: backchannel.com
Etag: 

<div id="zuckerberg-in-africa" class="pocketlog">
  <div><div class="excerpt-image">
      <img src="https://cdn-images-1.medium.com/max/1200/1*0BxY1jd6B1OA6edbRW1l1A.png" alt="Zuckerberg in Africa" />
    </div>
  <p>
      Mark Zuckerberg has been in Nigeria for barely an hour and is already rhapsodic. His remark does not reflect his biological heritage — obviously — but rather a connection based on the behavioral DNA that engineers share.
    </p>
    <p class="post-info">
      via <a href="https://backchannel.com/zuckerberg-in-africa-da3dabf74276?gi=7dba2179b3df">backchannel.com</a>
    </p>
  </div>
</div>