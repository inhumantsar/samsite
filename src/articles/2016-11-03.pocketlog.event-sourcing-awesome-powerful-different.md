Title: Event Sourcing: Awesome, powerful & different
Date: 2016-11-03 07:51
Tags: pocketlog, links, 
Slug: event-sourcing-awesome-powerful-different
Authors: www.erikheemskerk.nl
Etag: 

<div id="event-sourcing-awesome-powerful-different" class="pocketlog">
  <div>
  <p>
      I was introduced to Event Sourcing by the CQRS Journey series of articles from Microsoft's Patterns and Practices team. It’s an excellent read about a pattern that offers great flexibility, at a cost.
    </p>
    <p class="post-info">
      via <a href="https://www.erikheemskerk.nl/event-sourcing-awesome-powerful-different/">www.erikheemskerk.nl</a>
    </p>
  </div>
</div>