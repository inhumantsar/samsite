Title: About
Date: 2016-09-24 22:25
Slug: about
Authors: Shaun Martin

my background is in systems operations and, going way back, web development. lately i've been working in the software industry. tools are my thing. i like to build them. wrap logic around software to make less work. or at least, to make work less of a pain in the ass.

this site is supposed to be...

* loud
* a test bed for tinkering
* my vent
* part linkdump
* an idea hole
* [transparent](https://gitlab.com/inhumantsar/samsite)

and it's not supposed to be...

* representative of any company i do business with or am employed at
* Serious writing.
* a job ad (well, not entirely)
* professional blogging
* on wordpress anymore!
