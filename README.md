# samsite

my personal site. built with pelican, hosted on s3.

## cloudformation

the template included is mostly a copy/paste from the docs, but i've added notifications, logging, and a group/policy. manually created a user and added them to the DeployGroup.

## pelican

development container defaults sanely (for me).

## .gitlab-ci.yml

using gitlab's free ci service to deploy my site to s3. using reduced redundancy to save a little bit more cash. this is what the sns notification was for. might add a lambda job to fire a gitlab webhook. rebuild the site if anything goes missing.
