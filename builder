#!/usr/bin/env python
import builders.pocketlog as pl
import os
import string
import random
import argparse

# http://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
def id(size=6, chars=string.ascii_uppercase + string.digits):
  return ''.join(random.choice(chars) for _ in range(size))

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true')
parser.add_argument('--overwrite', action='store_true')
parser.add_argument('--no_archive', action='store_false', help='Do not archive processed source files.')
parser.add_argument('--bucket', default='samsite.ca')
parser.add_argument('--dest_path', default='drafts')

args = parser.parse_args()

for f in pl.dothething(bucket=args.bucket, debug=args.debug, archive=args.no_archive):
  d = os.path.join(args.dest_path,f['filename'])
  if os.path.exists(d) and not args.overwrite:
    d = '%s-%s' % (d, id())
  with open(d,'w') as fh:
    fh.write(f['content'].encode('utf-8'))
