FROM python:2
MAINTAINER shaun@samsite.ca

RUN mkdir /input /output /pelican-themes /pelican-plugins
VOLUME /input
VOLUME /output
VOLUME /pelican-themes
VOLUME /pelican-plugins

ENV THEME=/pelican-themes/Flex

RUN pip install pelican markdown typogrify pelican-flickr bs4 pelicanfly wand
RUN apt-get update; apt-get -y install npm nodejs nodejs-legacy
RUN npm install --global less less-plugin-clean-css

CMD lessc --clean-css $THEME/static/stylesheet/style.less $THEME/static/stylesheet/style.min.css  && pelican /input -v -o /output -t $THEME -s /conf.py
