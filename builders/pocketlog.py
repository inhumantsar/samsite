import boto3
import logging
import re
import os
import json
import botocore
from datetime import datetime, date
import dateutil.parser as dup
from jinja2 import Template as jt
import pytz
from multiprocessing import Pool as mpp
import pkg_resources
from slugify import slugify

logger = logging.getLogger()
s3 = None
now = None

def dothething(bucket, path='pocketlog/json', timezone='America/Vancouver', archive=True, debug=False):
  if debug:
    logger.setLevel(logging.DEBUG)

  if not len(logger.handlers):
    ch = logging.StreamHandler()
    logger.addHandler(ch)

  logger.info('pocketlog.dothething: starting...')
  global s3, now
  s3 = boto3.client('s3')
  now = datetime.now()

  r = []
  added = []
  for log in get_unadded_logs(bucket, path):
    for i in render_markdown(get_log(bucket, log['key'])):
      if i['content'] and i['slug']:
        r.append({'filename': format_filename(i['slug'], log['last_modified']), 'content': i['content']})
        added.append(log)

  if archive:
    archive_logs(bucket, added, timezone)

  logger.info('pocketlog.dothething: returning %s items.' % len(r))
  logger.debug('pocketlog.dothething: items: %s' % r)
  return r

def format_date(dt=None, format='%Y-%m-%d'):
  dt = now if not dt else dt
  if type(dt) in [str, unicode]:
    dt = dup.parse(dt)
  return dt.strftime(format)

def format_datetime(dt=None):
  return format_date(dt, '%Y-%m-%d %H:%M')

def format_slug(title):
  return slugify(title, max_length=75, word_boundary=True)

def format_filename(slug, dt=None):
  return '%s.pocketlog.%s.md' % (format_date(dt), slug)

def format_source_name(url):
  r = '^https?://([\w.]+)/'
  m = re.match(r,url)
  if m and len(m.groups()) > 0:
    return m.groups()[0]
  return 'source'

def get_log(bucket, key):
  '''fetch a reading log from s3'''
  try:
    body = s3.get_object(Bucket=bucket, Key=key)['Body'].read().decode('utf-8')
    logger.debug('get_log: {}'.format(body))
    return json.loads(body)
  except botocore.exceptions.ClientError as e:
    if e.response['Error']['Code'] == 'NoSuchKey':
      return None
    else:
      raise(e)

def read_markdown_header(path, lines=10):
  with open(path,'r') as f:
    h = [next(f) for x in xrange(lines)]
  return h

def get_unadded_logs(bucket, path):
  '''get all objects named like $path/*.json'''
  r = s3.list_objects_v2(Bucket=bucket, Prefix=path)
  return [{'key': i['Key'], 'etag': i['ETag'], 'last_modified': i['LastModified']} for i in r['Contents']]

def find_latest_date(dates):
  top = None
  for d in dates:
    if type(d) is str:
      d = dup.parse(d)
    if not top or d > top:
      top = d
  return top

def render_markdown(log, template='pocketlog.tmpl.md'):
  '''dump formatted markdown for a particular pocket log. outputting individual
  files for each article found.

  returns a list of dicts: [{'title':..., 'content':...}, ...]
  '''
  template = pkg_resources.resource_filename('builders', '/%s' % template)

  if len(log) == 0 or not log:
    logger.warn('render_markdown: no logs passed!')
    return None

  dt = find_latest_date([l['datetime'] for l in log if 'datetime' in l.keys()])
  dt = now if not dt else dt

  output = []
  for l in log:
    v = {
      'title': l['title'],
      'date': format_datetime(dt),
      'slug': format_slug(l['title']),
      'tags': ', '.join(set([ t.strip() for t in l['tags'].split(',')])),
      'etag': log[0]['etag'] if 'etag' in log[0].keys() else '',
      'source_name': format_source_name(l['url']),
      'image_url': '' if l['image_url'] == 'http://ifttt.com/images/no_image_card.png' else l['image_url'],
      'url': l['url'],
      'excerpt': l['excerpt']
    }

    output.append({
      'slug': format_slug(l['title']),
      'content': jt(open(template,'r').read()).render(**v),
    })
  return output

def archive_logs(bucket, logs, tz):
  '''consider anything already processed and older than today @ midnight
   (could still post new things today!) ripe for deletion. fuck it! trust in s3'''
  tz = pytz.timezone(tz)
  today = tz.localize(datetime.combine(date.today(), datetime.min.time()))
  dlist = [log for log in logs if log['last_modified'] < today]
  #### ennnhhh.. the bucket's versioned anyway.
  # for l in dlist:
  #   dest = os.path.join(os.path.dirname(l['key']), 'archive', os.path.basename(l['key']))
  #   logger.debug('archive_logs: (s3: %s)  copying %s to %s' % (bucket, l['key'], dest))
  #   try:
  #     s3.copy_object(Bucket=bucket, Key=dest, CopySource=l['key'])
  #   except botocore.exceptions.ClientError as e:
  #     logger.warn('archive_logs: unable to create archive copy of %s, skipping.' % l['key'])
  #     logger.warn('archive_logs: exception information: %s' % e)
  #     dlist.remove(l)
  ########
  if len(dlist) < 1:
    logger.debug('archive_logs: no log files on s3 ready for deletion')
    return []

  logger.info('archive_logs: (s3: %s)  archiving: %s' % (bucket, dlist))
  try:
    s3.delete_objects(Bucket=bucket, Delete={'Objects':[{'Key': l['key'] for l in dlist}]})
  except botocore.exceptions.ClientError as e:
    logger.warn('archive_logs: s3 delete operation failed: %s' % e)
  return dlist
