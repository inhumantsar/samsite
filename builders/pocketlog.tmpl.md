Title: {{ title }}
Date: {{ date }}
Tags: pocketlog, links, {{ tags }}
Slug: {{ slug }}
Authors: {{ source_name }}
Etag: {{ etag }}

<div id="{{ slug }}" class="pocketlog">
  {%- if image_url and excerpt == "" -%}
  <div class="image-only">
  {%- else %}
  <div>
  {%- endif %}
  {%- if image_url -%}
    <div class="excerpt-image">
      <img src="{{ image_url }}" alt="{{ title }}" />
    </div>
  {%- endif %}
  {% if excerpt != "" -%}
    <p>
      {{ excerpt }}
    </p>
  {%- endif %}
    <p class="post-info">
      via <a href="{{ url }}">{{ source_name }}</a>
    </p>
  </div>
</div>
